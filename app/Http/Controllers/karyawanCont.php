<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class karyawanCont extends Controller
{
    public function index() {

        $karyawan = DB::table('karyawan')->get();

        return view ('registrasi', ['kyn'=>$karyawan]);
    }

    public function create(Request $req) {
        $id = $req->id;
        $nama = $req->nama_karyawan;
        $no = $req->no_karyawan;
        $no_telp = $req->no_telp_karyawan;
        $divisi = $req->divisi_karyawan;
        $jabatan = $req->jabatan_karyawan;

        DB::table('karyawan')->insert([
            'nama_karyawan' => $nama,
            'no_karyawan' => $no,
            'no_telp_karyawan' => $no_telp,
            'divisi_karyawan' => $divisi,
            'jabatan_karyawan' => $jabatan
        ]);

        return redirect ('/registrasi')->with('success', 'Data berhasil ditambahkan.');
    }

    public function update($id){
        $karyawan = DB::table('karyawan')->find($id);

        return view ('/update', compact('karyawan'));
    }

    public function updatedata(Request $req, $id){
        $nama = $req->nama_karyawan;
        $no = $req->no_karyawan;
        $no_telp = $req->no_telp_karyawan;
        $jabatan = $req->jabatan_karyawan;
        $divisi = $req->divisi_karyawan;

        DB::table('karyawan')->where('id', $id)->update([
            'nama_karyawan' => $nama,
            'no_karyawan' => $no,
            'no_telp_karyawan' => $no_telp,
            'jabatan_karyawan' => $jabatan,
            'divisi_karyawan' => $divisi,
        ]);
        return redirect ('/registrasi')->with('success', 'Data berhasil diubah.');
    }

    public function delete($id)
    {

        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/registrasi')->with('success', 'Data berhasil dihapus.');
    }
}
