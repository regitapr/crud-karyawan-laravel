<?php

use Illuminate\Support\Facades\Route;


Route::get('/', [App\Http\Controllers\homeCont::class,'index'])->name('home');

Route::get('/registrasi', [App\Http\Controllers\karyawanCont::class,'index'])->name('registrasi');

Route::post('/create', [App\Http\Controllers\karyawanCont::class,'create'])->name('create');

Route::get('/update/{id}', [App\Http\Controllers\karyawanCont::class,'update'])->name('update');

Route::post('/updatedata/{id}', [App\Http\Controllers\karyawanCont::class,'updatedata'])->name('updatedata');

Route::get('/delete/{id}', [App\Http\Controllers\karyawanCont::class,'delete'])->name('delete');
