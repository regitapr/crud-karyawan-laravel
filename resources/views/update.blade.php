@extends('layout')

@section('judul')
    Home
@endsection
@section('konten')
<br>
<div class="container col-6 p-2">   
    <div class="card p-1">
      <div class="card-body">
        <h4 class="text-dark fw-bold text-center">Update Data Karyawan</h4>
        <form action="/updatedata/{{ $karyawan->id }}" method="POST" enctype="multipart/form">
          {{ csrf_field() }}
          <div class="mb-3">
            <label for="nama" class="form-label">Nama Karyawan</label>
            <input type="text" name="nama_karyawan" value="{{ $karyawan->nama_karyawan }}" class="form-control" id="nama"required>
          </div>
          <div class="row mb-3">
              <div class="col">
                  <label for="no_karyawan" class="form-label">No. Karyawan</label>
                  <input type="text" name="no_karyawan" value="{{ $karyawan->no_karyawan }}" class="form-control" id="nomor"required>
              </div>
              <div class="col">
                  <label for="telp" class="form-label">No. Telp Karyawan</label>
                  <input type="text" name="no_telp_karyawan" value="{{ $karyawan->no_telp_karyawan }}" class="form-control" id="telp"required>
              </div>
          </div>
          <div class="mb-3">
            <label for="jabatan" class="form-label">Jabatan Karyawan</label>
            <input type="text" name="jabatan_karyawan" value="{{ $karyawan->jabatan_karyawan }}" class="form-control" id="jabatan"required>
          </div>
          <div class="mb-3">
            <label for="divisi" class="form-label">Divisi Karyawan</label>
            <input type="text" name="divisi_karyawan" value="{{ $karyawan->divisi_karyawan }}" class="form-control" id="divisi"required>
          </div>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
@endsection