@extends('layout')

@section('judul')
    Registrasi
@endsection
@section('konten')
<br> 
<div class="container col-6 p-2">  
  @if($message = Session::get('success'))
  <div class="alert alert-success" role="alert">
  {{ $message }} 
  </div>
  @endif
  <div class="card p-1">
    <div class="card-body">
  </form>
      <h4 class="text-dark fw-bold text-center">Form Registrasi</h4>
      <form action="/create" method="post" enctype="multipart/form">
        {{ csrf_field() }}
        <div class="mb-3">
          <label for="nama" class="form-label">Nama Karyawan</label>
          <input type="text" name="nama_karyawan" class="form-control" id="nama"required>
        </div>
        <div class="row mb-3">
            <div class="col">
                <label for="no_karyawan" class="form-label">No. Karyawan</label>
                <input type="text" name="no_karyawan" class="form-control" id="nomor"required>
            </div>
            <div class="col">
                <label for="telp" class="form-label">No. Telp Karyawan</label>
                <input type="text" name="no_telp_karyawan" class="form-control" id="telp"required>
            </div>
        </div>
        <div class="mb-3">
          <label for="jabatan" class="form-label">Jabatan Karyawan</label>
          <input type="text" name="jabatan_karyawan" class="form-control" id="jabatan"required>
        </div>
        <div class="mb-3">
          <label for="divisi" class="form-label">Divisi Karyawan</label>
          <input type="text" name="divisi_karyawan" class="form-control" id="divisi"required>
        </div>
        <button type="submit" class="btn btn-primary">Registrasi</button>
      </div>
    </div>
  </div>
<hr>
<div class="container col-8 p-2">
  <div class="card p-1">
    <div class="card-body">
      <h4 class="text-dark fw-bold text-center p-1">Karyawan Terdaftar</h4>
      <table class="text-center table table-bordered table-hover">
        <thead class="table-primary">
          <tr>
            <th scope="col">id</th>
            <th scope="col">Nama Karyawan</th>
            <th scope="col">No. Karyawan</th>
            <th scope="col">No. Telp</th>
            <th scope="col">Jabatan</th>
            <th scope="col">Divisi</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        @foreach ($kyn as $k)
          <tbody>
            <tr>
              <td>{{ $k->id }}</td>
              <td>{{ $k->nama_karyawan }}</td>
              <td>{{ $k->no_karyawan }}</td>
              <td>{{ $k->no_telp_karyawan }}</td>
              <td>{{ $k->jabatan_karyawan }}</td>
              <td>{{ $k->divisi_karyawan }}</td>
              <td>
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                  <div class="btn-group me-2" role="group" aria-label="Second group">
                    <a href="/update/{{ $k->id }}" class="btn btn-warning">Edit</a>
                  </div>
                  <div class="btn-group" role="group" aria-label="Third group">
                    <a href="/delete/{{ $k->id }}" class="btn btn-danger">Delete</a>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        @endforeach
        </table>
    </div>
  </div>
</div>
@endsection